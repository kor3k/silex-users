<?php

namespace App\Controller;

use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type as Form;
use App\Entity\User;

class UserController extends \Core\AbstractController
{
    public function getRegisterAction()
    {
        return $this->app->render( '/user/registration.html.twig' , [ 'form' => $this->createRegistrationForm()->createView() ] );
    }

    public function postRegisterAction( Request $request )
    {
        $form   =   $this->createRegistrationForm();
        $form->submit( $request->request->get( 'registration' ) );

        if( $form->isValid() )
        {
            $user   =   $form->getData();
            $em     =   $this->app['orm.em'];

            $user->setPassword( $this->app->encodePassword( $user , $user->getPassword() ) );
            $em->persist( $user );
            $em->flush();

            return $this->app->render( '/user/registrationComplete.html.twig' , [ 'user' => $user ] );
        }
        else
        {
            return $this->app->render( '/user/registration.html.twig' , [ 'form' => $form->createView() ] );
        }
    }

    public function cgetAction()
    {
        $users  =   $this->getUserRepository()->findAll();

        return $this->app->render( '/user/list.html.twig' , [ 'users' => $users ] );
    }


    public function editAction( Request $request , $user )
    {
        $user   =   $this->getUserRepository()->findOneBy([ 'id' => $user ]);

        if( !$user )
        {
            $this->app->abort( 404 , 'uživatel nenalezen' );
        }

        $form   =   $this->createEditForm( $user );

        if( 'PUT' === $request->getMethod() )
        {
            $form->submit( $request->request->get( 'user' ) );

            if( $form->isValid() )
            {
                $user   =   $form->getData();
                $em     =   $this->app['doctrine']->getManager();

                $em->persist( $user );
                $em->flush();

                return $this->app->redirect( $this->app->url( 'edit_user' , [ 'user' => $user->getId() ] ) );
            }
        }

        return $this->app->render( '/user/edit.html.twig' , [
            'user'  =>  $user ,
            'form'  =>  $form->createView() ,
        ] );
    }

    public function deleteAction( $user )
    {
        $user   =   $this->getUserRepository()->findOneBy([ 'id' => $user ]);

        if( !$user )
        {
            $this->app->abort( 404 , 'uživatel nenalezen' );
        }

        $em     =   $this->app['orm.em'];
        $em->remove( $user );
        $em->flush();

        return $this->app->redirect( $this->app->url( 'get_users' ) );
    }

    protected function connect( \Silex\ControllerCollection $controllers )
    {
        $controllers->get( '/register', array( $this , 'getRegisterAction' ) )
            ->bind( 'get_register_user' )
        ;

        $controllers->post( '/register', array( $this , 'postRegisterAction' ) )
            ->bind( 'post_register_user' )
        ;

        $controllers->get( '/', array( $this , 'cgetAction' ) )
            ->bind( 'get_users' )
            ->secure( 'ROLE_ADMIN' )
        ;

        $controllers->get( '/{user}', array( $this , 'getAction' ) )
            ->bind( 'get_user' )
            ->secure( 'ROLE_ADMIN' )
        ;

        $controllers->put( '/{user}', array( $this , 'editAction' ) )
            ->bind( 'put_user' )
            ->secure( 'ROLE_ADMIN' )
        ;

        $controllers->delete( '/{user}', array( $this , 'deleteAction' ) )
            ->bind( 'delete_user' )
            ->secure( 'ROLE_ADMIN' )
        ;

        $controllers->get( '/{user}/edit', array( $this , 'editAction' ) )
            ->bind( 'edit_user' )
            ->secure( 'ROLE_ADMIN' )
        ;

        return $controllers;
    }


    /********************/

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getUserRepository()
    {
        return $this->app['orm.em']->getRepository( 'App\Entity\User' );
    }

    protected function createEditForm( User $user )
    {
        $roles  =   User::getAvailableRoles();
        $fb =   $this->app['form.factory']->createNamedBuilder( 'user' , Form\FormType::class , $user );
        $fb
            ->add( 'username' , Form\TextType::class  , [ 'disabled' => true , 'label' => 'user.username' ] )
            ->add( 'email' , Form\EmailType::class , [ 'disabled' => true ] )
            ->add( 'userRoles' , Form\ChoiceType::class  ,
                    [
                        'multiple'      => true ,
                        'constraints'   =>  [ new Constraints\Choice([ 'choices' => array_values( $roles ) , 'multiple' => true ]) ] ,
                        'choices'       =>  $roles ,
                        'label'         =>  'user.roles'
                    ])
            ->add( 'submit' , Form\SubmitType::class  , [ 'label'  =>  'user.save' ] )
        ;

        return $fb->getForm();
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function createRegistrationForm()
    {
        $fb =   $this->app['form.factory']->createNamedBuilder( 'registration' , Form\FormType::class , new User() );
        $fb
            ->add( 'email' , Form\EmailType::class ,
                    [
                        'constraints'   =>  [
                                                new Constraints\NotBlank() ,
                                                new Constraints\Email([ 'checkMX' => true ]) ,
                                                new Constraints\Callback(
                                                    function( $email , ExecutionContextInterface $context )
                                                    {
                                                        $user   =   $this->getUserRepository()->findOneBy([ 'email' => strtolower( $email ) ]);

                                                        if( $user )
                                                        {
                                                            $context->addViolation( "Email je již používán" );
                                                        }
                                                    }
                                                ) ,
                                            ] ,
                    ]
                 )
            ->add( 'username' , Form\TextType::class ,
                    [
                        'constraints'   =>  [
                                                new Constraints\NotBlank() ,
                                                new Constraints\Length([ 'min' => 3 , 'max' => '250' ]) ,
                                                new Constraints\Callback(
                                                    function( $username , ExecutionContextInterface $context )
                                                    {
                                                        $user   =   $this->getUserRepository()->findOneBy([ 'username' => strtolower( $username ) ]);

                                                        if( $user )
                                                        {
                                                            $context->addViolation( "Uživatelské jméno je již používáno" );
                                                        }
                                                    }
                                                ) ,
                                            ] ,
                        'label'     =>  'user.username' ,
                    ]
                 )
            ->add( 'password' , Form\RepeatedType::class ,
                    [
                        'type' => Form\PasswordType::class  ,
                        'constraints'   =>  [
                                                new Constraints\NotBlank() ,
                                                new Constraints\Length([ 'min' => 5 , 'max' => '250' ])
                                            ] ,
                        'invalid_message' => 'user.passwordNM',
                        'first_options'  => array('label' => 'user.password'),
                        'second_options' => array('label' => 'user.password2'),
                    ]
                )
            ->add( 'submit' , Form\SubmitType::class , [ 'label'  =>  'user.register' ] )
        ;

        return $fb->getForm();
    }
}